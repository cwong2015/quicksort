/**
 * Christopher Wong
 * 110410665
 * CSE 214 (4)
 * TA: Mingchen Zhang
 */

/**
 * 
 */
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Laptop {
	private String brand;
	private double procSpeed;
	private int memory;
	private int hdd;
	private static intComparatorClass intComparator = new intComparatorClass();
	private static brandComparatorClass brandComparator = new brandComparatorClass();
	private static procSpeedComparatorClass processorComparator = new procSpeedComparatorClass();
	private static memoryComparatorClass memoryComparator = new memoryComparatorClass();
	private static hddComparatorClass hddComparator = new hddComparatorClass();

	public Laptop(String brand, double procSpeed, int memory, int hdd) {
		this.brand = brand;
		this.procSpeed = procSpeed;
		this.memory = memory;
		this.hdd = hdd;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Input data type (must be 'int' or 'laptop'):");
		String inputDataType = scanner.nextLine().trim();
		if (!inputDataType.equals("int") && !inputDataType.equals("laptop"))
			throw new IllegalArgumentException("Invalid data type specified.");
		switch (inputDataType) {
		case "int":
			ArrayList<Integer> integers = readIntegerInputs(scanner);
			Sorter<Integer> intSorter = new Quicksorter<Integer>(intComparator, integers);
			intSorter.sort();
			System.out.println(getStringJoinedBy(integers, ", "));
			break;
		case "laptop":
			ArrayList<Laptop> laptops = readLaptopInputs(scanner);
			Sorter<Laptop> laptopSorter = new Quicksorter<>(brandComparator, laptops);
			laptopSorter.sort();
			System.out.print("Sorted by brand name:\n\t");
			System.out.println(getStringJoinedBy(laptops, "\n\t"));
			System.out.println();
			laptopSorter.setComparator(processorComparator);
			laptopSorter.sort();
			System.out.print("Sorted by processor speed:\n\t");
			System.out.println(getStringJoinedBy(laptops, "\n\t"));
			System.out.println();
			laptopSorter.setComparator(memoryComparator);
			laptopSorter.sort();
			System.out.print("Sorted by RAM:\n\t");
			System.out.println(getStringJoinedBy(laptops, "\n\t"));
			System.out.println();
			laptopSorter.setComparator(hddComparator);
			laptopSorter.sort();
			System.out.print("Sorted by hard disk capacity:\n\t");
			System.out.println(getStringJoinedBy(laptops, "\n\t"));
			break;
		default:
			throw new IllegalArgumentException("Invalid data type specified.");
		}
	}

	@SuppressWarnings("unchecked")
	private static String getStringJoinedBy(Object obj, String string) {
		ArrayList<Integer> integer;
		ArrayList<Laptop> laptop;
		String result = "";
		if (obj instanceof ArrayList<?>) {
			if (((ArrayList<?>) obj).get(0) instanceof Integer) {
				integer = (ArrayList<Integer>) obj;
				int i = 0;
				while (i < integer.size()) {
					if (i == integer.size()-1) {
						result += "" + integer.get(i);
						i++;
					} else {
						result += "" + integer.get(i) + string;
						i++;
					}
				}
				return result;
			}

			else if (((ArrayList<?>) obj).get(0) instanceof Laptop) {
				laptop = (ArrayList<Laptop>) obj;
				int i = 0;
				while (i < laptop.size()) {
					Laptop newLaptop = laptop.get(i);
					result += "{" + newLaptop.brand + ": " + newLaptop.procSpeed + " processor" + ", "
							+ newLaptop.memory + "GB RAM, " + newLaptop.hdd + "GB HDD}" + string;
					i++;
				}
				return result;
			}
		}
		return result;
	}

	@SuppressWarnings("rawtypes")
	private static class intComparatorClass implements Comparator {

		@Override
		public int compare(Object o1, Object o2) {
			int x = 0, y = 0;
			if (o1 instanceof Integer && o2 instanceof Integer) {
				x = (Integer) o1;
				y = (Integer) o2;
				if (x - y < 0) {
					return -1;
				}
				if (x - y > 0) {
					return 1;
				}
			}
			return 0;
		}

	}

	@SuppressWarnings("rawtypes")
	private static class brandComparatorClass implements Comparator {
		@Override
		public int compare(Object o1, Object o2) {
			Laptop x, y;
			String a = "", b = "";
			if (o1 instanceof Laptop && o2 instanceof Laptop) {
				x = (Laptop) o1;
				y = (Laptop) o2;
				a = x.brand;
				b = y.brand;
			}

			return (a.compareTo(b));
		}
	}

	@SuppressWarnings("rawtypes")
	private static class memoryComparatorClass implements Comparator {

		@Override
		public int compare(Object o1, Object o2) {
			Laptop a, b;
			int x, y;
			if (o1 instanceof Laptop && o2 instanceof Laptop) {
				a = (Laptop) o1;
				b = (Laptop) o2;
				x = a.memory;
				y = b.memory;
				if (x - y < 0) {
					return -1;
				}
				if (x - y > 0) {
					return 1;
				}
			}
			return 0;
		}

	}

	@SuppressWarnings("rawtypes")
	private static class hddComparatorClass implements Comparator {

		@Override
		public int compare(Object o1, Object o2) {
			Laptop a, b;
			int x, y;
			if (o1 instanceof Laptop && o2 instanceof Laptop) {
				a = (Laptop) o1;
				b = (Laptop) o2;
				x = a.hdd;
				y = b.hdd;
				if (x - y < 0) {
					return -1;
				}
				if (x - y > 0) {
					return 1;
				}
			}
			return 0;
		}

	}

	@SuppressWarnings("rawtypes")
	private static class procSpeedComparatorClass implements Comparator {

		@Override
		public int compare(Object o1, Object o2) {
			Laptop x, y;
			double a = 0, b = 0;
			if (o1 instanceof Laptop && o2 instanceof Laptop) {
				x = (Laptop) o1;
				y = (Laptop) o2;
				a = x.procSpeed;
				b = y.procSpeed;
				if (a - b < 0) {
					return -1;
				}
				if (a - b > 0) {
					return 1;
				}
			}
			return 0;
		}

	}

	private static ArrayList<Integer> readIntegerInputs(Scanner scanner) {
		ArrayList<Integer> intList = new ArrayList<Integer>();
		Scanner intScanner = scanner;
		String input = intScanner.nextLine();
		while (!input.equals("end")) {
			int a = Integer.parseInt(input);
			intList.add(a);
			input = intScanner.nextLine();
		}
		return intList;
	}

	private static ArrayList<Laptop> readLaptopInputs(Scanner scanner) {
		ArrayList<Laptop> laptopList = new ArrayList<Laptop>();
		Scanner laptopScanner = scanner;
		String input = laptopScanner.nextLine();
		while (!input.equals("end")) {
			Laptop laptop;
			String[] arr = input.split(",");
			String theBrand = arr[0];
			double theProcSpeed = Double.parseDouble(arr[1]);
			int theMemory = Integer.parseInt(arr[2]);
			int theHHD = Integer.parseInt(arr[3]);
			laptop = new Laptop(theBrand, theProcSpeed, theMemory, theHHD);
			laptopList.add(laptop);
			input = laptopScanner.nextLine();
		}
		return laptopList;

	}
}