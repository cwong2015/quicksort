/**
 * Christopher Wong
 * 110410665
 * CSE 214 (4)
 * TA: Mingchen Zhang
 */

/**
 * 
 */
import java.util.Comparator;
import java.util.ArrayList;

public class Quicksorter<E> implements Sorter<E> {
	private Comparator<E> comparator;
	private ArrayList<E> listOfThings;

	public Quicksorter(Comparator<E> comparator, ArrayList<E> listOfThings) {
		this.comparator = comparator;
		this.listOfThings = listOfThings;
	}

	@Override
	public void sort() {
		int first = 0;
		int last = listOfThings.size() - 1;
		sort(listOfThings, first, last);
	}

	private void sort(ArrayList<E> listOfThings2, int first, int last) {
		int left, right;
		E pivot;
		if (first >= last) {
			return;
		}
		left = first;
		right = last;

		pivot = listOfThings.get(left + (right - left) / 2);

		while (left <= right) {

			while (compare(listOfThings.get(left), pivot) < 0) {
				left++;
			}
			while (compare(listOfThings.get(right), pivot) > 0) {
				right--;
			}
			if (left <= right) {
				swap(listOfThings, left, right);
				left++;
				right--;
			}
		}

		sort(listOfThings, first, right);
		sort(listOfThings, left, last);

	}

	private void swap(ArrayList<E> inputList, int left, int right) {
		E temp = inputList.get(left);
		inputList.set(left, inputList.get(right));
		inputList.set(right, temp);

	}

	@Override
	public void setComparator(Comparator<E> comparator) {
		this.comparator = comparator;

	}

	@SuppressWarnings("unchecked")
	private int compare(E a, E b) {
		if (comparator == null) {
			return ((Comparable<E>) a).compareTo(b);
		} else {
			return comparator.compare(a, b);
		}
	}

}
