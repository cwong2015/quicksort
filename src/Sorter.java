/**
 * Christopher Wong
 * 110410665
 * CSE 214 (4)
 * TA: Mingchen Zhang
 */

/**
 * 
 */
import java.util.Comparator;

public interface Sorter<E> {
	void sort();

	void setComparator(Comparator<E> comparator);
}
